//Let's simulate a set of names and ages
const names = {
	"Brandon" : {
		"name" : "Brandon Boyd",
		"age" : 35
	},
	"Steve": {
		"name" : "Steve Tyler",
		"age" : 56
	}

}


function factorial(n){
	// if(typeof n !== 'number') return undefined;
	// if(n<0) return undefined;
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
}

//comment out div_check
// function div_check(n){

// 	if(n%5 === 0) return true
// 	if(n%7 === 0) return true
// 	return false

// }

//export names variable
module.exports = {
	factorial: factorial,
	names: names
	// div_check: div_check
}
